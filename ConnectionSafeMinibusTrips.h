#include <map>
#include <stack>
#include <utility>
#include "Vertex.h"
#include "SCC.h"

#ifndef CONNECTION_SAFE_MINIBUS_TRIPS_CONNECTIONSAFEMINIBUSTRIPS_H
#define CONNECTION_SAFE_MINIBUS_TRIPS_CONNECTIONSAFEMINIBUSTRIPS_H


class ConnectionSafeMinibusTrips {
private:
    int num;
    int N;
    map<int, SCC*> sccMap;
    map<int, Vertex*> vertices;

    Vertex* getNextVertex(Edge* edge);
    Vertex* getVertex(Edge* edge);
    SCC* getNextSCC(Edge* edge);
    void topologicalSortUtil(SCC* scc, stack<SCC*>& stack);
    stack<SCC*> topologicalSort();
    void findScc(Vertex* vertex);

public:
    ConnectionSafeMinibusTrips(int iN, map<int, Vertex*> iVertices)
        : N(iN), vertices(std::move(iVertices)), num(0) {}

    int evaluate();
};


#endif //CONNECTION_SAFE_MINIBUS_TRIPS_CONNECTIONSAFEMINIBUSTRIPS_H
