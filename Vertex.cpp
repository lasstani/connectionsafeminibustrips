#include <vector>
#include "Vertex.h"

int Vertex::getLabel() const {
    return this->label;
}

int Vertex::getIndex() const {
    return this->index;
}

int Vertex::getLowLink() const {
    return this->lowLink;
}

bool Vertex::isInStack() const {
    return this->inStack;
}

bool Vertex::isSave() const {
    return this->save;
}

void Vertex::setLabel(int iLabel) {
    this->label = iLabel;
}

void Vertex::setIndex(int iIndex) {
    this->index = iIndex;
}

void Vertex::setLowlink(int iLowlink) {
    this->lowLink = iLowlink;
}

void Vertex::onStack() {
    this->inStack = true;
}

void Vertex::notInStack() {
    this->inStack = false;
}

void Vertex::saved() {
    this->save = true;
}

void Vertex::notSave() {
    this->save = false;
}

vector<Edge*> Vertex::getEdges() {
    return this->edges;
}

void Vertex::addEdge(Edge *edge) {
    this->edges.push_back(edge);
}

Vertex::~Vertex() {
    for(auto edge: this->edges){
        delete(edge);
    }
}
