#include <vector>
#include "Edge.h"

#ifndef CONNECTION_SAFE_MINIBUS_TRIPS_VERTEX_H
#define CONNECTION_SAFE_MINIBUS_TRIPS_VERTEX_H


using namespace std;

class Vertex {
private:
    int label;
    int index;
    int lowLink;
    bool inStack;
    bool save;
    vector<Edge*> edges;

public:
    Vertex(int iLabel) : label(iLabel){
        index = lowLink = -1;
        inStack = false;
        save = true;
    }

    ~Vertex();

    int getLabel() const;
    int getIndex() const;
    int getLowLink() const;
    bool isInStack() const;
    bool isSave() const;

    void setLabel(int iLabel);
    void setIndex(int iIndex);
    void setLowlink(int iLowlink);
    void onStack();
    void notInStack();
    void saved();
    void notSave();

    void addEdge(Edge* edge);
    vector<Edge*> getEdges();
};

#endif //CONNECTION_SAFE_MINIBUS_TRIPS_VERTEX_H
