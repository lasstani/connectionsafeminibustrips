#include <iostream>
#include <fstream>
#include <map>
#include "Vertex.h"
#include "ConnectionSafeMinibusTrips.h"

using namespace std;

int N, M;

// print --help
void printHelpPage(char *program) {
    cout << "Connection-Safe Minibus Trips:" << endl;
    cout << "\tCalculate the maximum number of connection-safe stops that can be visited in one minibus trip." << endl;
    cout << endl << "Usage:" << endl;
    cout << "\t" << program << " [options]" << endl << endl;
    cout << "General options:" << endl;
    cout << "\t--input-file INPUT_PATH, -if INPUT_PATH" << endl;
    cout << "\t\tFile with input data" << endl;
    cout << "\t--output-file OUTPUT_PATH, -of OUTPUT_PATH" << endl;
    cout << "\t\tGenerates file with solution." << endl;
    cout << "\t--help, -h" << endl;
    cout << "\t\tPrints this help." << endl;
}

// read input stream and parse it into map of vertices
// if stream is not in specified format throw exception
map<int, Vertex*> readInput(istream& stream) {
    map<int, Vertex*> vertices;
    int a, b;

    try {
        stream >> N >> M;
        for (int i = 0; i < N; i++) {
            vertices[i] = new Vertex(i);
        }

        for (int i = 0; i < M; i++) {
            stream >> a >> b;
            Edge *edge = new Edge(a, b);
            vertices[edge->getA()]->addEdge(edge);
        }
    }
    catch (...){
       throw runtime_error("Parsing error!");
    }

    return vertices;
}

void writeSolution(ostream& stream, int solution) {
    stream << solution << endl;
}

int main(int argc, char *argv[]) {
    string inputFile, outputFile;

    // parse program parameters. Supported params:
    // --help/-h - print how to use program
    // --input-file/-if - followed by path to the input file
    // --output-file/-of - followed by path to the output file
    // other parameters are not supported and this throw exception
    for (int i = 1; i < argc; ++i) {
        string parg = argv[i];
        if (parg == "--help" || parg == "-h") {
            printHelpPage(argv[0]);
            return 0;
        }

        if (parg == "--input-file" || parg == "-if") {
            if (i + 1 < argc) {
                inputFile = argv[++i];
            } else {
                throw runtime_error("Expected a filename for the input data!");
            }
        }

        if (parg == "--output-file" || parg == "-of") {
            if (i + 1 < argc) {
                outputFile = argv[++i];
            } else {
                throw runtime_error("Expected a filename for the solution!");
            }
        }

        if (!parg.empty() && parg[0] != '-') {
            printHelpPage(argv[0]);
            throw runtime_error("Unsupported parameter!");
        }
    }

    // decide which input type will be used and read input
    map<int, Vertex*> vertices;
    if (!inputFile.empty()) {
        ifstream stream(inputFile);
        vertices = readInput(stream);
    }
    else {
        vertices = readInput(cin);
    }

    // create ConnectionSafeMinibusTrips instance and evaluate solution
    ConnectionSafeMinibusTrips connectionSafeMinibusTrips(N, vertices);
    auto solution = connectionSafeMinibusTrips.evaluate();

    // decide which output type will be used and write solution
    if (!outputFile.empty()) {
        ofstream stream(outputFile);
        writeSolution(stream, solution);
    }
    else {
        writeSolution(cout, solution);
    }

    return 0;
}
