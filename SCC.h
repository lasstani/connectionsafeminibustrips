#include <vector>
#include "Edge.h"
#include "Vertex.h"

#ifndef CONNECTION_SAFE_MINIBUS_TRIPS_SCC_H
#define CONNECTION_SAFE_MINIBUS_TRIPS_SCC_H

class SCC {
private:
    int save;
    int maxSave;
    int label;
    bool visited;
    vector<Vertex*> vertices;
    vector<Edge*> edges;

public:
    explicit SCC(int iLabel) : label(iLabel), save(0), maxSave(0), visited(false) {}

    int getSave() const;
    int getMaxSave() const;
    int getLabel() const;
    bool isVisited() const;
    vector<Vertex*> getVertices();
    vector<Edge*> getEdges();

    void subSaved();
    void setMaxSaved(int iMaxSaved);
    void addMaxSaved();
    void subMaxSaved();
    void wasVisited();
    void addEdge(Edge *edge);
    void addVertex(Vertex *vertex);
};

#endif //CONNECTION_SAFE_MINIBUS_TRIPS_SCC_H
