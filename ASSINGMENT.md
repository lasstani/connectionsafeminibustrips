# Connection-Safe Minibus Trips
The Lake District Travel Agency operates in the Lake District which is famous for its natural wonders and strict environmental rules limiting access to the area.
Any motorized access to places in Lake District is possible only by licensed minibusses. There is a number of minibus stops. Typically, a stop is built in a vicinity of some natural point of interest. The roads are extremely narrow, thus each route is only one-directional. The transport is organized by so-called minibus connections.
A minibus connection always connects just two different minibus stops - the start stop and the end stop of that connection.

Due to the weather and wildlife conditions, varying in time, the minibus traffic on the connections in the area might be a bit erratic sometimes. Thus, an additional property of minibus stops is defined.
A minibus connection C from stop a to stop b is considered to be an unsafe connection if and only if a traveller who leaves a via C cannot return to a by any sequence of minibus connections.
All minibus stops in which no unsafe connection starts or ends are considered to be connection-safe stops.
No other minibus stops are considered to be connection-safe.

A minibus trip is a sequence of minibus connections in which the start stop of a minibus connection is also the end stop of the previous minibus connection in the sequence. Any minibus stop and any minibus connection may appear more times in a minibus trip, the number of times a particular minibus stop or a particular minibus connection appears in a minibus trip is not limited.

The agency plans to offer minibus trips that visit the maximum possible number of connection-safe stops. Each visited connection-safe stop is counted only once in a trip, regardless of the number of visits to the stop.

![Figure 1](data/img.png)

Figure 1. A scheme of minibus stops and connections. There are 17 stops and 24 connections. The connection-safe stops are highlighted in blue. There is a minibus trip that visits connection-safe stops 3, 4, 9, 10, 11, 8, 16. There is no minibus trip which visits more than 7 connection-safe stops. The scheme illustrates Example 1 below.

# The task
You are given the list of all minibus connections in the Lake District. Calculate the maximum number of connection-safe stops that can be visited in one minibus trip.