#include "SCC.h"

int SCC::getSave() const {
    return this->save;
}

int SCC::getMaxSave() const {
    return this->maxSave;
}

int SCC::getLabel() const {
    return this->label;
}

bool SCC::isVisited() const {
    return this->visited;
}

vector<Vertex *> SCC::getVertices() {
    return this->vertices;
}

vector<Edge *> SCC::getEdges() {
    return this->edges;
}

void SCC::subSaved() {
    this->save--;
}

void SCC::addMaxSaved() {
    this->maxSave++;
}

void SCC::subMaxSaved() {
    this->maxSave--;
}

void SCC::wasVisited() {
    this->visited = true;
}

void SCC::addEdge(Edge *edge) {
    this->edges.push_back(edge);
}

void SCC::setMaxSaved(int maxSaved) {
    this->maxSave = maxSaved;
}

void SCC::addVertex(Vertex *vertex) {
    this->save++;
    this->vertices.push_back(vertex);
}
