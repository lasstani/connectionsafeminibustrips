# Connection-Safe Minibus Trips

## Zadanie:
Kompletné zadanie nájdete v súbore [ASSINGMENT.md](ASSINGMENT.md).

## Úloha
Nájdite maximálny počet bezpečných zastávok, ktoré môžu byť navštívené pri jednej ceste autobusom.

---

## Implemantácia
Použité boli dva algoritmy: 
1. [Tarjanov algoritmus](https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm) na hľadanie silne súvislých komponent
2. [Topologické usporiadanie](https://en.wikipedia.org/wiki/Topological_sorting#:~:text=In%20computer%20science%2C%20a%20topological,before%20v%20in%20the%20ordering.&text=Topological%20sorting%20has%20many%20applications,such%20as%20feedback%20arc%20set.) na zoradenie komponent

Najprv pomocov [tarjanovho algoritmu](https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm) nájdeme všetky silne súvislé komponenty grafu. Počas hľadania si značíme, ktoré vrcholy sú bezpečné a taktiež si pamätáme ich počet. 
Následne usporiadame komponenty pomocou [topologického usporiadania](https://en.wikipedia.org/wiki/Topological_sorting#:~:text=In%20computer%20science%2C%20a%20topological,before%20v%20in%20the%20ordering.&text=Topological%20sorting%20has%20many%20applications,such%20as%20feedback%20arc%20set.) a hľadáme cestu, ktorá má maximálny počet bezpečných zastávok.
Na hľadanie cesty sme použili [dynamické programovanie](https://en.wikipedia.org/wiki/Dynamic_programming).

---

## Zostavenia a spustenie
#### Build:
```
mkdir build
cd build
cmake ..
make
```

#### Run:
```
./Connection_Safe_Minibus_Trips [options]
```

Program je možné pustiť bez parametrov. Vtedy užívateľ zadáva vstup z príkazového riadku a výsledok vypíše na štandardný výstup.
Program prijíma parametre, ktoré je potrebne aktivovať príslušnými prepínačmi. Prepínač
`--input-file` alebo `-if`, po ktorom nasleduje parameter `INPUT_PATH` a prepínač `--output-file` alebo `-of`, po ktorom nasleduje parameter `OUTPUT_PATH`.
Pokiaľ sú tieto parametre špecifikované vstupné data sa berú zo súboru a výsledok sa vypíše do súboru.

Program obsahuje prepínač `--help` alebo `-h`, ktorý vypíše nápovedu ako je potrebné spustiť program.

---

## Vstupné data
Program očakáva na začiatku 2 čísla `N` a `M`, ktoré predstavujú počet vrcholov a počet hrán v grafe.
Nasleduje `M` riadkov, ktoré sa skladajú z 2 čísel a predsatavujú hrany v grafe. Hrany sú zadávané pomocou indexu počiatočného vrcholu a indexu koncového vrcholu.

Príklad vstupných dát je v zložke [data/inputs](data/inputs). A príklad správnych riešení je v [data/solutions](data/solutions).