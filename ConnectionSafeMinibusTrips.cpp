#include "ConnectionSafeMinibusTrips.h"

// return end vertex of edge
Vertex *ConnectionSafeMinibusTrips::getNextVertex(Edge *edge) {
    return this->vertices[edge->getB()];
}

// return starting vertex of edge
Vertex *ConnectionSafeMinibusTrips::getVertex(Edge *edge) {
    return this->vertices[edge->getA()];
}

// return SCC where edge ends
SCC *ConnectionSafeMinibusTrips::getNextSCC(Edge *edge) {
    return this->sccMap[vertices[edge->getB()]->getLowLink()];
}

// recursive function used by topologicalSort
void ConnectionSafeMinibusTrips::topologicalSortUtil(SCC *scc, stack<SCC *> &stack) {
    scc->wasVisited();

    // recur for all SCCs adjacent to this SCC
    for(auto v: scc->getVertices()) {
        for (auto edge: v->getEdges()) {
            auto vertex = getVertex(edge);
            auto nextVertex = getNextVertex(edge);

            if (vertex->getLowLink() != nextVertex->getLowLink()) {
                auto nextScc = getNextSCC(edge);

                // if vertex is safe reduce number of safe vertices in SCC
                if (vertex->isSave()) {
                    scc->subSaved();
                }

                if (nextVertex->isSave()) {
                    nextScc->subSaved();
                }

                // make sure that if vertex is connected to other SCC then it is not safe
                vertex->notSave();
                nextVertex->notSave();

                // add edge to other SCC
                scc->addEdge(edge);

                // if next scc is not visited call recursively
                if (!nextScc->isVisited()) {
                    topologicalSortUtil(nextScc, stack);
                }
            }
        }
    }

    scc->setMaxSaved(scc->getSave());

    // if next scc is not push SCC into stack which stores result
    stack.push(scc);
}

stack<SCC *> ConnectionSafeMinibusTrips::topologicalSort() {
    stack<SCC*> stack;

    // call the recursive helper function to store Topological
    // sort starting from all vertices one by one
    for(auto v: sccMap){
        if(!v.second->isVisited()){
            topologicalSortUtil(v.second, stack);
        }
    }

    return stack;
}

// find all strongly connected components
void ConnectionSafeMinibusTrips::findScc(Vertex *vertex) {
    static stack<Vertex*> s;

    num++;
    vertex->setIndex(num);
    vertex->setLowlink(num);

    // put vertex on stack
    s.push(vertex);
    vertex->onStack();

    // go through all vertices adjacent to this
    for(auto edge: vertex->getEdges()){
        Vertex* v = getNextVertex(edge);

        // if v is not visited yet, then recur for it
        if(v->getIndex() < 0){
            findScc(v);
            // check if the subtree rooted with 'v' has a connection to one of the ancestors of 'vertex'
            vertex->setLowlink(min(vertex->getLowLink(), v->getLowLink()));
        }

        // update lowLink of 'vertex' only of 'v' is still in stack
        else if(v->isInStack()){
            vertex->setLowlink(min(vertex->getLowLink(), v->getIndex()));
        }
    }

    // head vertex found, pop the stack
    if(vertex->getLowLink() == vertex->getIndex()){
        auto scc = new SCC(vertex->getLowLink());
        while(true){
            auto v = s.top();s.pop();
            v->notInStack();
            v->setLowlink(vertex->getLowLink());
            scc->addVertex(v);
            if(vertex == v){
                break;
            }
        }

        // add vertex into sccMap on position of vertex lowLink
        sccMap[vertex->getLowLink()] = scc;
    }
}

int ConnectionSafeMinibusTrips::evaluate() {
    auto maximum = 0;

    // call the recursive helper function to find strongly connected components
    for(int i = 0; i < this->N; i++) {
        if(vertices[i]->getLowLink() < 0)
            findScc(vertices[i]);
    }

    // run topological sort
    auto stack = topologicalSort();

    // go through all SCC and find path with the biggest number of safe vertices
    while(!stack.empty()){
        auto scc = stack.top(); stack.pop();
        for(auto edge: scc->getEdges()){
            auto nextScc = getNextSCC(edge);
            nextScc->setMaxSaved(max(nextScc->getSave() + scc->getMaxSave(), nextScc->getMaxSave()));
        }
        maximum = max(maximum, scc->getMaxSave());
        delete(scc);
    }

    // clean up vertices
    for(int i = 0; i < N; i++){
        delete(vertices[i]);
    }

    return maximum;
}
