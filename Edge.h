#ifndef CONNECTION_SAFE_MINIBUS_TRIPS_EDGE_H
#define CONNECTION_SAFE_MINIBUS_TRIPS_EDGE_H


class Edge {
private:
    int a;
    int b;

public:
    Edge(int iA, int iB) : a(iA), b(iB) {}

    int getA() const;
    int getB() const;
};


#endif //CONNECTION_SAFE_MINIBUS_TRIPS_EDGE_H
